package subject;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface subjectRepository extends CrudRepository<subject, String> {
	  

	  List<subject> findBytenMon(String tenMon);

	  List<subject> findBymaMon(String maMon);
	}
