package subject;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

public class subjectController {
	@Autowired
	  subjectService subjectService;

	  @RequestMapping(method = RequestMethod.GET, value = "/subject/{maMon}")
	  public Optional<subject> getSubjectInfo(@PathVariable String maMon) {
	    return subjectService.getSubject(maMon);
	  }

	  @RequestMapping(value="/subject/{maMon}", method=RequestMethod.PUT)
	  public void updateSubject(@PathVariable String maMon,String tenMon, String BMPhutrach, String gioiThieu, @RequestBody subject sj) {
	      subjectService.updateSubject(maMon, tenMon, BMPhutrach, gioiThieu, sj);
	  }

	  @RequestMapping(value = "/subject/{maMon}", method = RequestMethod.POST)
	  public void createsubject(@PathVariable String  maMon,String tenMon, String BMPhutrach, String gioiThieu, @RequestBody subject sj) {
	    subjectService.createSubject(maMon, tenMon, BMPhutrach, gioiThieu, sj);
	  }

	  @RequestMapping(value = "/subject", method = RequestMethod.GET)
	  public List<subject> getAllSubjects() {
		  return subjectService.getAllsubjects();
	  }

	  @RequestMapping(value = "/subject/search", method = RequestMethod.GET)
	  public List<subject> searchBymaMon(@RequestParam String maMon) {
	    return subjectService.findsubjectBymaMon(maMon);
	  }

	  @RequestMapping(value = "/subject/advance_search", method = RequestMethod.GET)
	  public List<subject> searchBytenMon(@RequestParam String tenMon) {
	    return subjectService.findsubjectBytenMon(tenMon);
	  }
}
